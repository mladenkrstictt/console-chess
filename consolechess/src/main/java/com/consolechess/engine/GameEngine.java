package com.consolechess.engine;

import java.util.Scanner;

import com.consolechess.board.Board;
import com.consolechess.exceptions.IllegalInputException;
import com.consolechess.exceptions.IllegalPieceMoveException;
import com.consolechess.inputparser.InputParser;
import com.consolechess.inputparser.Move;
import com.consolechess.renderer.GameRenderer;

public enum GameEngine {

	INSTANCE;
	
	private final String SURRENDER_COMMAND = "I SURRENDER";
	
	public void runGame() {
		
		boolean someoneSurrendered = false;
    	
    	try(Scanner scanner = new Scanner(System.in)) {
    		
    		System.out.println(getWellcomeMessageWithTutorial());
    		
    		System.out.println("Enter name of the player with white pieces:");
        	String whitePlayerName = scanner.nextLine();
          
        	System.out.println("Enter name of the player with black pieces:");
        	String blackPlayerName = scanner.nextLine();
        	
    		Board board = new Board(whitePlayerName, blackPlayerName);
    		GameRenderer renderer = new GameRenderer();
    		InputParser inputParser = new InputParser();
    		
    		while(!someoneSurrendered) {
    			
    			renderer.render(board);
    			
    			System.out.println(board.isWhitePlayerTurn() ? board.getWhitePlayerName() + "'s move:"
    					: board.getBlackPlayerName() + "'s move:");
    			String move = scanner.nextLine();
    			if(move.trim().equalsIgnoreCase(SURRENDER_COMMAND)) {
    				someoneSurrendered = true;
    			} else {
    				Move parsedMove = null;
    				while(parsedMove == null) {
    					try {
    						parsedMove = inputParser.parseMove(move);
    					} catch (IllegalInputException e) {
    						System.out.println(e.getMessage());
    						move = scanner.nextLine();
    					}
    					if(parsedMove != null) {
	    					try {
								board.move(parsedMove.getFrom(), parsedMove.getTo());
							} catch (IllegalPieceMoveException e) {
								System.out.println(e.getMessage());
	    						move = scanner.nextLine();
	    						if(move.trim().equalsIgnoreCase(SURRENDER_COMMAND)) {
	    		    				someoneSurrendered = true;
	    		    				break;
	    		    			}
	    						parsedMove = null;
							}
    					}
    				}
    				
    				renderer.clearConsole();
    			}
    		}
    	}
	}
	
	private String getWellcomeMessageWithTutorial() {
		
		return "\nWelcome to the console chess game. When it's your turn to play you should\n"
				+ "type your move using convention such as \"a2 to a4\", \"c1 to e3\" end so on...\n"
				+ "If you want to give up because of a checkmat or any other reason, just type \"I surrender\"\n"
				+ "instead of a standard move.\n"
				+ "If you type invalid input or unproper move that is not allowed under chess rules, you will\n"
				+ "see the validation warning. You can always correct yourself after any of those warnings.\n";
	}
}
