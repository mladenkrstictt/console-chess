package com.consolechess.board.piece.movevalidator;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.exceptions.IllegalPieceMoveException;

public class KnightMoveValidator extends AbstractPieceMoveValidator {

	public KnightMoveValidator(Board board) {
		super(board);
	}

	@Override
	protected void validateMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		int rowAbsDifference = Math.abs(from.getRow() - to.getRow());
		int columnAbsDifference = Math.abs(from.getColumn() - to.getColumn());
		
		boolean isPathValid = (rowAbsDifference == 2 && columnAbsDifference == 1) 
				|| (rowAbsDifference == 1 && columnAbsDifference == 2);
		
		if(!isPathValid) {
			throw new IllegalPieceMoveException("Knight can only go two steps horizontally follwed by one step vertically "
					+ "or one step horizontally follwed by two steps vertically.");
		}
	}

}
