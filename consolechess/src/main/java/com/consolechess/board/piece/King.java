package com.consolechess.board.piece;

import com.consolechess.board.Board;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.movevalidator.KingMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.board.piece.reachablefieldscalculator.KingReachableFieldsCalculator;

public class King extends AbstractPiece {

	public King(PieceColor color) {
		super(color);
	}

	@Override
	protected AbstractPieceMoveValidator getMoveValidator(Board board) {
		
		return new KingMoveValidator(board);
	}

	@Override
	protected AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board) {
		
		return new KingReachableFieldsCalculator(board);
	}
}
