package com.consolechess.board.piece.reachablefieldscalculator;

import static com.consolechess.board.piece.PieceColor.BLACK;
import static com.consolechess.board.piece.PieceColor.WHITE;

import java.util.HashSet;
import java.util.Set;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.PieceColor;

public class PawnReachableFieldsCalculator extends AbstractReachableFieldsCalculator {

	public PawnReachableFieldsCalculator(Board board) {
		super(board);
	}

	@Override
	public Set<BoardField> calculateReachableFields(BoardField initialField) {
		
		PieceColor pawnColor = board.getPieceOnPosition(initialField).getColor();
		if(pawnColor == PieceColor.WHITE) {
			return calculateReachableFieldsForWhitePawn(initialField);
		} 
		return calculateReachableFieldsForBlackPawn(initialField);
	}

	private Set<BoardField> calculateReachableFieldsForWhitePawn(BoardField initialField) {
		
		Set<BoardField> reachableFields = new HashSet<>();
		
		boolean isPawnInInitialPosition = initialField.getRow() == 6;
		
		BoardField candidateField1 = new BoardField(initialField.getRow() - 1, initialField.getColumn());
		BoardField candidateField2 = new BoardField(initialField.getRow() - 2, initialField.getColumn());
		BoardField candidateField3 = new BoardField(initialField.getRow() - 1, initialField.getColumn() + 1);
		BoardField candidateField4 = new BoardField(initialField.getRow() - 1, initialField.getColumn() - 1);
		
		if(isFieldInRange(candidateField1) && board.getPieceOnPosition(candidateField1) == null) {
			reachableFields.add(candidateField1);
		}
		
		if(isFieldInRange(candidateField2) && isPawnInInitialPosition 
				&& board.getPieceOnPosition(candidateField2) == null && board.getPieceOnPosition(candidateField1) == null) {
			
			reachableFields.add(candidateField2);
		}
		
		if(isFieldInRange(candidateField3) && board.getPieceOnPosition(candidateField3) != null
				&& board.getPieceOnPosition(candidateField3).getColor() == BLACK) {
			
			reachableFields.add(candidateField3);
		}
		
		if(isFieldInRange(candidateField4) && board.getPieceOnPosition(candidateField4) != null
				&& board.getPieceOnPosition(candidateField4).getColor() == BLACK) {
			
			reachableFields.add(candidateField4);
		}
		
		return reachableFields;
	}
	
	private Set<BoardField> calculateReachableFieldsForBlackPawn(BoardField initialField) {
		
		Set<BoardField> reachableFields = new HashSet<>();
		
		boolean isPawnInInitialPosition = initialField.getRow() == 1;
		
		BoardField candidateField1 = new BoardField(initialField.getRow() + 1, initialField.getColumn());
		BoardField candidateField2 = new BoardField(initialField.getRow() + 2, initialField.getColumn());
		BoardField candidateField3 = new BoardField(initialField.getRow() + 1, initialField.getColumn() + 1);
		BoardField candidateField4 = new BoardField(initialField.getRow() + 1, initialField.getColumn() - 1);
		
		if(isFieldInRange(candidateField1) && board.getPieceOnPosition(candidateField1) == null) {
			reachableFields.add(candidateField1);
		}
		
		if(isFieldInRange(candidateField2) && isPawnInInitialPosition 
				&& board.getPieceOnPosition(candidateField2) == null && board.getPieceOnPosition(candidateField1) == null) {
			
			reachableFields.add(candidateField2);
		}
		
		if(isFieldInRange(candidateField3) && board.getPieceOnPosition(candidateField3) != null
				&& board.getPieceOnPosition(candidateField3).getColor() == WHITE) {
			
			reachableFields.add(candidateField3);
		}
		
		if(isFieldInRange(candidateField4) && board.getPieceOnPosition(candidateField4) != null
				&& board.getPieceOnPosition(candidateField4).getColor() == WHITE) {
			
			reachableFields.add(candidateField4);
		}
		
		return reachableFields;
	}
}
