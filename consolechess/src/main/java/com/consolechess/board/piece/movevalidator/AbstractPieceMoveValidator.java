package com.consolechess.board.piece.movevalidator;

import static com.consolechess.board.piece.PieceColor.BLACK;
import static com.consolechess.board.piece.PieceColor.WHITE;

import java.security.cert.PKIXRevocationChecker.Option;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.AbstractPiece;
import com.consolechess.exceptions.IllegalPieceMoveException;

public abstract class AbstractPieceMoveValidator {

	protected Board board = null;
	
	public AbstractPieceMoveValidator(Board board) {
		super();
		this.board = board;
	}
	
	protected abstract void validateMove(BoardField from, BoardField to)
			throws IllegalPieceMoveException;
	
	private void commonValidation(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		AbstractPiece currentPiece = board.getPieceOnPosition(from);
		if(currentPiece == null) {
			throw new IllegalPieceMoveException("From position doesn't contain any piece!");
		}
		if(from.getRow() == to.getRow() && from.getColumn() == to.getColumn()) {
			throw new IllegalPieceMoveException("Piece didn't actually move");
		}
		if(board.isWhitePlayerTurn() && currentPiece.getColor() != WHITE) {
			throw new IllegalPieceMoveException("It's " + board.getWhitePlayerName() + "'s turn but the black piece was on a \"from\" field." );
		}
		if((!board.isWhitePlayerTurn()) && currentPiece.getColor() != BLACK) {
			throw new IllegalPieceMoveException("It's " + board.getBlackPlayerName() + "'s turn but the white piece was on a \"from\" field." );
		}
		if(board.getPieceOnPosition(from).getColor() ==  
				(board.getPieceOnPosition(to) == null ? null : board.getPieceOnPosition(to).getColor())) {
			throw new IllegalPieceMoveException("Target position contains your own piece.");
		}
	}
	
	public void validate(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		commonValidation(from, to);
		validateMove(from, to);
	}
	
}
