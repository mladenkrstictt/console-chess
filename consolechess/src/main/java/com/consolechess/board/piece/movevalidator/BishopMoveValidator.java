package com.consolechess.board.piece.movevalidator;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.AbstractPiece;
import com.consolechess.exceptions.IllegalPieceMoveException;

public class BishopMoveValidator extends AbstractPieceMoveValidator {

	public BishopMoveValidator(Board board) {
		super(board);
	}

	@Override
	protected void validateMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		if(Math.abs(from.getRow() - to.getRow()) != Math.abs(from.getColumn() - to.getColumn())) {
			
			throw new IllegalPieceMoveException("Bishop can move only by diagonal.");
		}
		
		int rowStepSize = to.getRow() < from.getRow() ? -1 : 1;
		int columnStepSize = to.getColumn() < from.getColumn() ? -1 : 1;
		
		BoardField currentField = new BoardField(from.getRow() + rowStepSize, from.getColumn() + columnStepSize);
		while(currentField.getRow() != to.getRow()) {
			AbstractPiece pieceOnCurrentPosition = board.getPieceOnPosition(currentField);
			if(pieceOnCurrentPosition != null) {
				throw new IllegalPieceMoveException("Bishop can't jump over other pieces.");
			}
			currentField = new BoardField(currentField.getRow() + rowStepSize, currentField.getColumn() + columnStepSize);
		}
	}

}
