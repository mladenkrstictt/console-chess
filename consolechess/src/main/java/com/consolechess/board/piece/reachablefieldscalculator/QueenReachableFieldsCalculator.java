package com.consolechess.board.piece.reachablefieldscalculator;

import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.EAST;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.NORTH;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.NORTH_EAST;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.NORTH_WEST;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.SOUTH;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.SOUTH_EAST;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.SOUTH_WEST;
import static com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator.Direction.WEST;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.AbstractPiece;

public class QueenReachableFieldsCalculator extends AbstractReachableFieldsCalculator {

	public QueenReachableFieldsCalculator(Board board) {
		super(board);
	}

	@Override
	public Set<BoardField> calculateReachableFields(BoardField initialField) {
		
		Set<BoardField> reachableFields = new HashSet<>();
		
		List<Direction> directions = Arrays.asList(NORTH, SOUTH, WEST, EAST, NORTH_WEST, NORTH_EAST, SOUTH_WEST, SOUTH_EAST);
		
		for(Direction direction : directions) {
			int rowStep = direction.getRowStep();
			int columnStep = direction.getColumnStep();
			
			BoardField currentField = new BoardField(initialField.getRow() + rowStep, initialField.getColumn() + columnStep);
			while(isFieldInRange(currentField)) {
				AbstractPiece pieceOnCurrentPosition = board.getPieceOnPosition(currentField);
				if(pieceOnCurrentPosition != null) {
					if(pieceOnCurrentPosition.getColor() != board.getPieceOnPosition(initialField).getColor()) {
						reachableFields.add(currentField);
					}
					break;
				}
				reachableFields.add(currentField);
				currentField = new BoardField(currentField.getRow() + rowStep, currentField.getColumn() + columnStep);
			}
		}
		
		return reachableFields;
	}

}
