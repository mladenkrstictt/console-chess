package com.consolechess.board.piece;

import com.consolechess.board.Board;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.movevalidator.BishopMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.board.piece.reachablefieldscalculator.BishopReachableFieldsCalculator;

public class Bishop extends AbstractPiece {

	public Bishop(PieceColor color) {
		super(color);
	}

	@Override
	protected AbstractPieceMoveValidator getMoveValidator(Board board) {
		
		return new BishopMoveValidator(board);
	}

	@Override
	protected AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board) {
		
		return new BishopReachableFieldsCalculator(board);
	}
}
