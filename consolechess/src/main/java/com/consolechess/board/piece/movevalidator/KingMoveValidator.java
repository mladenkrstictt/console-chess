package com.consolechess.board.piece.movevalidator;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.exceptions.IllegalPieceMoveException;

public class KingMoveValidator extends AbstractPieceMoveValidator {

	public KingMoveValidator(Board board) {
		super(board);
	}

	@Override
	protected void validateMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		int rowAbsDifference = Math.abs(from.getRow() - to.getRow());
		int columnAbsDifference = Math.abs(from.getColumn() - to.getColumn());
		
		boolean isPathValid = rowAbsDifference <= 1 && columnAbsDifference <= 1;
		
		if(!isPathValid) {
			throw new IllegalPieceMoveException("King can move only one step vertically, horizontally or diagonally");
		}
	}

}
