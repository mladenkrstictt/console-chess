package com.consolechess.board.piece.movevalidator;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.AbstractPiece;
import com.consolechess.exceptions.IllegalPieceMoveException;

public class QueenMoveValidator extends AbstractPieceMoveValidator {

	public QueenMoveValidator(Board board) {
		super(board);
	}

	@Override
	protected void validateMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		boolean isDiagonalMove = Math.abs(from.getRow() - to.getRow()) == Math.abs(from.getColumn() - to.getColumn());
		boolean isVerticalOrHorizontalMove = from.getRow() == to.getRow() || from.getColumn() == to.getColumn();
		
		if((!isDiagonalMove) && (!isVerticalOrHorizontalMove)) {
			throw new IllegalPieceMoveException("Queen can move only vertically, horizontally or diagonally");
		}
		
		int rowStepSize = to.getRow() < from.getRow() ? -1 : (to.getRow() == from.getRow() ? 0 : 1);
		int columnStepSize = to.getColumn() < from.getColumn() ? -1 : (to.getColumn() == from.getColumn() ? 0 : 1);
		
		BoardField currentField = new BoardField(from.getRow() + rowStepSize, from.getColumn() + columnStepSize);
		while(currentField.getRow() != to.getRow()) {
			AbstractPiece pieceOnCurrentPosition = board.getPieceOnPosition(currentField);
			if(pieceOnCurrentPosition != null) {
				throw new IllegalPieceMoveException("Queen can't jump over other pieces.");
			}
			currentField = new BoardField(currentField.getRow() + rowStepSize, currentField.getColumn() + columnStepSize);
		}
	}

}
