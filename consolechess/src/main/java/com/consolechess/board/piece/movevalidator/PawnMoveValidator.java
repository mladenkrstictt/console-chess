package com.consolechess.board.piece.movevalidator;

import static com.consolechess.board.piece.PieceColor.BLACK;
import static com.consolechess.board.piece.PieceColor.WHITE;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.PieceColor;
import com.consolechess.exceptions.IllegalPieceMoveException;

public class PawnMoveValidator extends AbstractPieceMoveValidator {

	public PawnMoveValidator(Board board) {
		super(board);
	}

	@Override
	protected void validateMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		PieceColor pawnColor = board.getPieceOnPosition(from).getColor();
		if(pawnColor == WHITE) {
			validateWhitePawnMove(from, to);
		} else {
			validateBlackPawnMove(from, to);
		}
	}
	
	private void validateWhitePawnMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		boolean pawnInInitialPosition = from.getRow() == 6;
		
		boolean validPossibility1 = to.getRow() - from.getRow() == -1 && from.getColumn() == to.getColumn() && board.getPieceOnPosition(to) == null;
		
		boolean validPossibility2 = pawnInInitialPosition && to.getRow() - from.getRow() == -2 
				&& from.getColumn() == to.getColumn() 
				&& board.getPieceOnPosition(to) == null 
				&& board.getPieceOnPosition(new BoardField(to.getRow() + 1, to.getColumn())) == null;
		
		boolean validPossibility3 = to.getRow() - from.getRow() == -1 && Math.abs(from.getColumn() - to.getColumn()) == 1 
				&& board.getPieceOnPosition(to) != null && board.getPieceOnPosition(to).getColor() == BLACK;
		
		if((!validPossibility1) && (!validPossibility2) && (!validPossibility3)) {
			throw new IllegalPieceMoveException("Pawn can move only one step forward vertically or diagonally if it will capture oponent's piece. \n"
					+ "It can also move 2 steps forward vertically if it is in initial position and have no obstacles.");
		}
	}

	private void validateBlackPawnMove(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		boolean pawnInInitialPosition = from.getRow() == 1;
		
		boolean validPossibility1 = to.getRow() - from.getRow() == 1 && from.getColumn() == to.getColumn() && board.getPieceOnPosition(to) == null;
		
		boolean validPossibility2 = pawnInInitialPosition && to.getRow() - from.getRow() == 2 
				&& from.getColumn() == to.getColumn() 
				&& board.getPieceOnPosition(to) == null 
				&& board.getPieceOnPosition(new BoardField(to.getRow() - 1, to.getColumn())) == null;
		
		boolean validPossibility3 = to.getRow() - from.getRow() == 1 && Math.abs(from.getColumn() - to.getColumn()) == 1 
				&& board.getPieceOnPosition(to) != null && board.getPieceOnPosition(to).getColor() == WHITE;
		
		if((!validPossibility1) && (!validPossibility2) && (!validPossibility3)) {
			throw new IllegalPieceMoveException("Pawn can move only one step forward vertically or diagonally if it will capture oponent's piece. \n"
					+ "It can also move 2 steps forward vertically if it is in initial position and have no obstacles.");
		}
	}
}
