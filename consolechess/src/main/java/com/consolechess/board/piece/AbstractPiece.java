package com.consolechess.board.piece;

import java.util.Set;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.exceptions.IllegalPieceMoveException;

public abstract class AbstractPiece {

	private final PieceColor color;
	
	public AbstractPiece(PieceColor color) {
		
		this.color = color;
	}

	public PieceColor getColor() {
		return color;
	}

	public void validateMove(Board board, BoardField from, BoardField to)
			throws IllegalPieceMoveException {
		
		AbstractPieceMoveValidator validator = getMoveValidator(board);
		validator.validate(from, to);
	}
	
	public boolean canReachField(Board board, BoardField initialField, BoardField targetField) {
		
		AbstractReachableFieldsCalculator reachableFieldsCalculator = getReachableFieldsCalculator(board);
		Set<BoardField> reachableFields = reachableFieldsCalculator.calculateReachableFields(initialField);
		return reachableFields.contains(targetField);
	}
	
	protected abstract AbstractPieceMoveValidator getMoveValidator(Board board);
	
	protected abstract AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board);
}
