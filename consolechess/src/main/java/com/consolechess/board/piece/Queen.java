package com.consolechess.board.piece;

import com.consolechess.board.Board;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.movevalidator.QueenMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.board.piece.reachablefieldscalculator.QueenReachableFieldsCalculator;

public class Queen extends AbstractPiece {

	public Queen(PieceColor color) {
		super(color);
	}

	@Override
	protected AbstractPieceMoveValidator getMoveValidator(Board board) {
		
		return new QueenMoveValidator(board);
	}

	@Override
	protected AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board) {
		
		return new QueenReachableFieldsCalculator(board);
	}
}
