package com.consolechess.board.piece.reachablefieldscalculator;

import java.util.Set;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;

public abstract class AbstractReachableFieldsCalculator {

	protected Board board = null;
	
	public AbstractReachableFieldsCalculator(Board board) {
		this.board = board;
	}
	
	public abstract Set<BoardField> calculateReachableFields(BoardField initialField);
	
	protected boolean isFieldInRange(BoardField field) {
		
		if(field.getRow() >= 0 && field.getRow() <= 7 && field.getColumn() >= 0 && field.getColumn() <= 7) {
			return true;
		}
		return false;
	}
	
	protected static enum Direction {
		
		NORTH(-1, 0),
		SOUTH(1, 0),
		WEST(0, -1),
		EAST(0, 1),
		NORTH_WEST(-1, -1),
		NORTH_EAST(-1, 1),
		SOUTH_WEST(1, -1),
		SOUTH_EAST(1, 1);
		
		private final int rowStep;
		private final int columnStep;
		
		private Direction(int rowStep, int columnStep) {
			this.rowStep = rowStep;
			this.columnStep = columnStep;
		}

		public int getRowStep() {
			return rowStep;
		}

		public int getColumnStep() {
			return columnStep;
		}
	}
}
