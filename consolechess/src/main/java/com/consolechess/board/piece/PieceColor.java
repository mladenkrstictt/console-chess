package com.consolechess.board.piece;

public enum PieceColor {
	
	BLACK,
	WHITE;
}
