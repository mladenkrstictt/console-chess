package com.consolechess.board.piece;

import com.consolechess.board.Board;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.movevalidator.PawnMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.board.piece.reachablefieldscalculator.PawnReachableFieldsCalculator;

public class Pawn extends AbstractPiece {

	public Pawn(PieceColor color) {
		super(color);
	}

	@Override
	protected AbstractPieceMoveValidator getMoveValidator(Board board) {
		
		return new PawnMoveValidator(board);
	}

	@Override
	protected AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board) {
		
		return new PawnReachableFieldsCalculator(board);
	}
}
