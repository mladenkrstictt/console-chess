package com.consolechess.board.piece.reachablefieldscalculator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;

public class KingReachableFieldsCalculator extends AbstractReachableFieldsCalculator {

	public KingReachableFieldsCalculator(Board board) {
		super(board);
	}

	@Override
	public Set<BoardField> calculateReachableFields(BoardField initialField) {
		
		Set<BoardField> reachableFields = new HashSet<>();
		
		List<BoardField> candidateFields = new ArrayList<>();
		candidateFields.add(new BoardField(initialField.getRow() - 1, initialField.getColumn() - 1));
		candidateFields.add(new BoardField(initialField.getRow() - 1, initialField.getColumn()));
		candidateFields.add(new BoardField(initialField.getRow() - 1, initialField.getColumn() + 1));
		candidateFields.add(new BoardField(initialField.getRow() + 1, initialField.getColumn() - 1));
		candidateFields.add(new BoardField(initialField.getRow() + 1, initialField.getColumn()));
		candidateFields.add(new BoardField(initialField.getRow() + 1, initialField.getColumn() + 1));
		candidateFields.add(new BoardField(initialField.getRow(), initialField.getColumn() - 1));
		candidateFields.add(new BoardField(initialField.getRow(), initialField.getColumn() + 1));
		
		for(BoardField candidateField : candidateFields) {
			if(isFieldInRange(candidateField)) {
				if(board.getPieceOnPosition(candidateField) == null || 
						board.getPieceOnPosition(candidateField).getColor() != board.getPieceOnPosition(initialField).getColor()) {
					reachableFields.add(candidateField);
				}
			}
		}
		
		return reachableFields;
	}

}
