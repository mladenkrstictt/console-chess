package com.consolechess.board.piece;

import com.consolechess.board.Board;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.movevalidator.RookMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.board.piece.reachablefieldscalculator.RookReachableFieldsCalculator;

public class Rook extends AbstractPiece {

	public Rook(PieceColor color) {
		super(color);
	}
	
	@Override
	protected AbstractPieceMoveValidator getMoveValidator(Board board) {
		
		return new RookMoveValidator(board);
	}

	@Override
	protected AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board) {
		
		return new RookReachableFieldsCalculator(board);
	}

}
