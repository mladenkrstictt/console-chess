package com.consolechess.board.piece;

import com.consolechess.board.Board;
import com.consolechess.board.piece.movevalidator.AbstractPieceMoveValidator;
import com.consolechess.board.piece.movevalidator.KnightMoveValidator;
import com.consolechess.board.piece.reachablefieldscalculator.AbstractReachableFieldsCalculator;
import com.consolechess.board.piece.reachablefieldscalculator.KnightReachableFieldsCalculator;

public class Knight extends AbstractPiece {

	public Knight(PieceColor color) {
		super(color);
	}

	@Override
	protected AbstractPieceMoveValidator getMoveValidator(Board board) {
		
		return new KnightMoveValidator(board);
	}

	@Override
	protected AbstractReachableFieldsCalculator getReachableFieldsCalculator(Board board) {
		
		return new KnightReachableFieldsCalculator(board);
	}
}
