package com.consolechess.board;

import static com.consolechess.board.piece.PieceColor.BLACK;
import static com.consolechess.board.piece.PieceColor.WHITE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.consolechess.board.piece.AbstractPiece;
import com.consolechess.board.piece.Bishop;
import com.consolechess.board.piece.King;
import com.consolechess.board.piece.Knight;
import com.consolechess.board.piece.Pawn;
import com.consolechess.board.piece.PieceColor;
import com.consolechess.board.piece.Queen;
import com.consolechess.board.piece.Rook;
import com.consolechess.exceptions.IllegalPieceMoveException;

public class Board {

	private AbstractPiece[][] matrix = {
			
		{ new Rook(BLACK), new Knight(BLACK), new Bishop(BLACK), new Queen(BLACK), new King(BLACK),
			new Bishop(BLACK), new Knight(BLACK), new Rook(BLACK) },
		
		{ new Pawn(BLACK), new Pawn(BLACK), new Pawn(BLACK), new Pawn(BLACK), new Pawn(BLACK),
			new Pawn(BLACK), new Pawn(BLACK), new Pawn(BLACK) },
		
		{ null, null, null, null, null, null, null, null },
		
		{ null, null, null, null, null, null, null, null },
		
		{ null, null, null, null, null, null, null, null },
		
		{ null, null, null, null, null, null, null, null },
		
		{ new Pawn(WHITE), new Pawn(WHITE), new Pawn(WHITE), new Pawn(WHITE), new Pawn(WHITE),
			new Pawn(WHITE), new Pawn(WHITE), new Pawn(WHITE) },
		
		{ new Rook(WHITE), new Knight(WHITE), new Bishop(WHITE), new Queen(WHITE), new King(WHITE),
			new Bishop(WHITE), new Knight(WHITE), new Rook(WHITE) }
	};
	
	private Set<BoardField> positionsWithWhitePieces = new HashSet<>();
	private Set<BoardField> positionsWithBlackPieces = new HashSet<>();
	
	private List<AbstractPiece> capturedWhitePieces = new ArrayList<>();
	private List<AbstractPiece> capturedBlackPieces = new ArrayList<>();
	
	private String whitePlayerName;
	private String blackPlayerName;
	private boolean whitePlayerTurn = true;
	
	private BoardField currentWhiteKingPosition = new BoardField(7, 4);
	private BoardField currentBlackKingPosition = new BoardField(0, 4);
	
	public Board(String whitePlayerName, String blackPlayerName) {
		
		this.whitePlayerName = whitePlayerName;
		this.blackPlayerName = blackPlayerName;
		
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[0].length; j++) {
				AbstractPiece piece = matrix[i][j];
				if(piece != null) {
					if(piece.getColor() == WHITE) {
						positionsWithWhitePieces.add(new BoardField(i, j));
					} else {
						positionsWithBlackPieces.add(new BoardField(i, j));
					}
				}
			}
		}
	}
	
	public void move(BoardField from, BoardField to) throws IllegalPieceMoveException {
		
		AbstractPiece currentPiece = getPieceOnPosition(from);
		if(currentPiece == null) {
			throw new IllegalPieceMoveException("From position doesn't contain any piece!");
		}
		currentPiece.validateMove(this, from, to);
		
		AbstractPiece potentiallyCapturedPiece = getPieceOnPosition(to);
		setPieceOnPosition(null, from);
		setPieceOnPosition(currentPiece, to);
		updateKingPositionIfNeeded(currentPiece, to);
		
		if(isKingUnderAttack(isWhitePlayerTurn() ? WHITE : BLACK)) {
			//undo move
			setPieceOnPosition(currentPiece, from);
			setPieceOnPosition(potentiallyCapturedPiece, to);
			updateKingPositionIfNeeded(currentPiece, from);
			throw new IllegalPieceMoveException("Your king would be under attack after this move.");
		}
		
		trackCapturedPieces(potentiallyCapturedPiece);
		whitePlayerTurn = !whitePlayerTurn;
	}
	
	private boolean isKingUnderAttack(PieceColor kingColor) {
		
		BoardField targetField = kingColor == WHITE ? currentWhiteKingPosition : currentBlackKingPosition;
		
		Set<BoardField> potentialAttackerPositions = kingColor == WHITE ? 
				positionsWithBlackPieces : positionsWithWhitePieces;
		Iterator<BoardField> iterator = potentialAttackerPositions.iterator();
		while(iterator.hasNext()) {
			BoardField potentialAttackerPosition = iterator.next();
			AbstractPiece potentialAttacker = matrix[potentialAttackerPosition.getRow()][potentialAttackerPosition.getColumn()];
			if(potentialAttacker.canReachField(this, potentialAttackerPosition, targetField)) {
				return true;
			}
		}
		return false;
	}
	
	private void updateKingPositionIfNeeded(AbstractPiece piece, BoardField position) {
		
		if(piece instanceof King) {
			if(WHITE == piece.getColor()) {
				currentWhiteKingPosition.setRow(position.getRow());
				currentWhiteKingPosition.setColumn(position.getColumn());
			} else {
				currentBlackKingPosition.setRow(position.getRow());
				currentBlackKingPosition.setColumn(position.getColumn());
			}
		}
	}
	
	private void trackCapturedPieces(AbstractPiece potentiallyCapturedPiece) {
		
		if(potentiallyCapturedPiece != null) {
			if(potentiallyCapturedPiece.getColor() == WHITE) {
				capturedWhitePieces.add(potentiallyCapturedPiece);
			} else {
				capturedBlackPieces.add(potentiallyCapturedPiece);
			}
		}
	}
	
	public boolean isWhitePlayerTurn() {
		return whitePlayerTurn;
	}
	
	public AbstractPiece getPieceOnPosition(BoardField position) {
		return matrix[position.getRow()][position.getColumn()];
	}

	private void setPieceOnPosition(AbstractPiece piece, BoardField position) {
		AbstractPiece previousPiece = matrix[position.getRow()][position.getColumn()];
		matrix[position.getRow()][position.getColumn()] = piece;
		if(previousPiece != null) {
			if(previousPiece.getColor() == WHITE) {
				positionsWithWhitePieces.remove(position);
			} else {
				positionsWithBlackPieces.remove(position);
			}
		}
		if(piece != null) {
			if(piece.getColor() == WHITE) {
				positionsWithWhitePieces.add(position);
			} else {
				positionsWithBlackPieces.add(position);
			}
		}
	}
	
	public String getWhitePlayerName() {
		return whitePlayerName;
	}

	public void setWhitePlayerName(String whitePlayerName) {
		this.whitePlayerName = whitePlayerName;
	}

	public String getBlackPlayerName() {
		return blackPlayerName;
	}

	public void setBlackPlayerName(String blackPlayerName) {
		this.blackPlayerName = blackPlayerName;
	}
	
	public List<AbstractPiece> getCapturedWhitePieces() {
		//We return safe copy because we don't want to allow other classes to modify internal state of the board.
		//Pieces are immutable so there is no problem with exposing their references.
		return new ArrayList<>(capturedWhitePieces);
	}
	
	public List<AbstractPiece> getCapturedBlackPieces() {
		//We return safe copy because we don't want to allow other classes to modify internal state of the board.
		//Pieces are immutable so there is no problem with exposing their references.
		return new ArrayList<>(capturedBlackPieces);
	}
}
