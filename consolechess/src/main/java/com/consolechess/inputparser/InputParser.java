package com.consolechess.inputparser;

import java.text.MessageFormat;

import com.consolechess.board.BoardField;
import com.consolechess.exceptions.IllegalInputException;

public class InputParser {

	private static final String INVALID_MOVE_FORMAT_MESSAGE = 
			"Your input {0} is does not satisfy the move format specification. Try again.";
	
	public Move parseMove(String move) throws IllegalInputException {
		
		move = move.toUpperCase();
		
		String errorMessage = MessageFormat.format(INVALID_MOVE_FORMAT_MESSAGE, new Object[] { move });
		IllegalInputException invalidFormatException = new IllegalInputException(errorMessage);
		
		if(move == null) {
			throw new IllegalInputException("Input is not set.");
		}
		String[] parts = move.split("TO");
		if(parts.length != 2) {
			throw invalidFormatException;
		}
		
		String fromMove = parts[0].trim();
		String toMove = parts[1].trim();
		
		if(fromMove.length() != 2 || toMove.length() != 2) {
			throw invalidFormatException;
		}
		
		int fromMoveRow = fromMove.charAt(0) - 'A';
		int fromMoveColumn = fromMove.charAt(1) - '1';
		
		int toMoveRow = toMove.charAt(0) - 'A';
		int toMoveColumn = toMove.charAt(1) - '1';
		
		if(isInRange(fromMoveRow) && isInRange(fromMoveColumn) && isInRange(toMoveRow) && isInRange(toMoveColumn)) {
			return new Move(
				new BoardField(fromMoveRow, fromMoveColumn), 
				new BoardField(toMoveRow, toMoveColumn));
		}
		
		throw invalidFormatException;
	}
	
	private boolean isInRange(int rowOrColumnIndex) {
		return rowOrColumnIndex >= 0 && rowOrColumnIndex <= 7;
	}
}
