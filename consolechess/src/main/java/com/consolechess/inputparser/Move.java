package com.consolechess.inputparser;

import com.consolechess.board.BoardField;

public class Move {

	private BoardField from, to;

	public Move(BoardField from, BoardField to) {
		this.from = from;
		this.to = to;
	}

	public BoardField getFrom() {
		return from;
	}

	public void setFrom(BoardField from) {
		this.from = from;
	}

	public BoardField getTo() {
		return to;
	}

	public void setTo(BoardField to) {
		this.to = to;
	}
}
