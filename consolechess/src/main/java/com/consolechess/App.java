package com.consolechess;

import com.consolechess.engine.GameEngine;

public class App {
	
    public static void main( String[] args ) {
    	
    	GameEngine.INSTANCE.runGame();
    }
}
