package com.consolechess.exceptions;

public class IllegalInputException extends Exception {

	private static final long serialVersionUID = 3779056278948122085L;

	public IllegalInputException() {
		super("");
	}
	
	public IllegalInputException(String message) {
		super(message);
	}
}
