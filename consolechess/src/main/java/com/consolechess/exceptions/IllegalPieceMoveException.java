package com.consolechess.exceptions;

public class IllegalPieceMoveException extends Exception {
	
	private static final long serialVersionUID = -1164186356154202307L;
	
	public IllegalPieceMoveException() {
		super("");
	}
	
	public IllegalPieceMoveException(String message) {
		super(message);
	}
	
}
