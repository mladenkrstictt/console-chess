package com.consolechess.renderer.piecerenderer;

import java.util.HashMap;
import java.util.Map;

import com.consolechess.board.piece.AbstractPiece;
import com.consolechess.board.piece.Bishop;
import com.consolechess.board.piece.King;
import com.consolechess.board.piece.Knight;
import com.consolechess.board.piece.Pawn;
import com.consolechess.board.piece.PieceColor;
import com.consolechess.board.piece.Queen;
import com.consolechess.board.piece.Rook;

public abstract class PieceRenderer {

	private static final Map<Class<? extends AbstractPiece>, PieceRenderer> rendererByPieceType = new HashMap<>(); 
		
	static {
		rendererByPieceType.put(Pawn.class, new PawnRenderer());
		rendererByPieceType.put(Rook.class, new RookRenderer());
		rendererByPieceType.put(Knight.class, new KnightRenderer());
		rendererByPieceType.put(Bishop.class, new BishopRenderer());
		rendererByPieceType.put(King.class, new KingRenderer());
		rendererByPieceType.put(Queen.class, new QueenRenderer());
	}
	
	public abstract String render(PieceColor color);
	
	public static String renderPiece(AbstractPiece piece) {
		
		if(piece == null) {
			return "   ";
		}
		
		PieceRenderer renderer = rendererByPieceType.get(piece.getClass());
		return renderer.render(piece.getColor());
	}
}
