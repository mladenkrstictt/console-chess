package com.consolechess.renderer.piecerenderer;

import com.consolechess.board.piece.PieceColor;

public class RookRenderer extends PieceRenderer {

	@Override
	public String render(PieceColor color) {
		
		return (color == PieceColor.WHITE ? "w" : "b") + "r ";
	}

}
