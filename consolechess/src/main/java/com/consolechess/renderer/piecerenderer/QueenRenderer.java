package com.consolechess.renderer.piecerenderer;

import com.consolechess.board.piece.PieceColor;

public class QueenRenderer extends PieceRenderer {

	@Override
	public String render(PieceColor color) {
		
		return (color == PieceColor.WHITE ? "w" : "b") + "q ";
	}

}
