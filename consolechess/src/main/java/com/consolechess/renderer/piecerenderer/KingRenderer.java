package com.consolechess.renderer.piecerenderer;

import com.consolechess.board.piece.PieceColor;

public class KingRenderer extends PieceRenderer {

	@Override
	public String render(PieceColor color) {
		
		return (color == PieceColor.WHITE ? "w" : "b") + "ki";
	}

}
