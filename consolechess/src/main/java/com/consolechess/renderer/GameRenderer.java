package com.consolechess.renderer;

import java.util.List;

import com.consolechess.board.Board;
import com.consolechess.board.BoardField;
import com.consolechess.board.piece.AbstractPiece;
import com.consolechess.renderer.piecerenderer.PieceRenderer;

public class GameRenderer {

	public void render(Board board) {

		renderHeader(board);
		renderCapturedPieces(board);
		renderColumnNumbers(board);
		for (int i = 0; i < 8; i++) {
			renderRow(board, i);
		}
		System.out.println();
		System.out.println();
	}

	public void clearConsole() {
		
		for (int i = 0; i < 1000; i++) {
			System.out.println();
		}
	}
	
	private void renderCapturedPieces(Board board) {
		
		List<AbstractPiece> capturedWhitePieces = board.getCapturedWhitePieces();
		List<AbstractPiece> capturedBlackPieces = board.getCapturedBlackPieces();
		
		System.out.println("Captured white pieces:");
		capturedWhitePieces.stream().forEach(piece -> System.out.print(PieceRenderer.renderPiece(piece) + "  "));
		System.out.println();
		System.out.println();
		
		System.out.println("Captured black pieces:");
		capturedBlackPieces.stream().forEach(piece -> System.out.print(PieceRenderer.renderPiece(piece) + "  "));
		System.out.println();
		System.out.println();
	}
	
	private void renderHeader(Board board) {
		
		System.out.println(board.getWhitePlayerName() + " vs " + board.getBlackPlayerName());
		System.out.println();
	}
	
	private void renderColumnNumbers(Board board) {
		
		System.out.print("   ");
		for (int i = 0; i < 8; i++) {

			System.out.print("  " + (i + 1) + "   ");
		}
		System.out.println();
		
		System.out.print("  ");
		for (int i = 0; i < 8; i++) {

			System.out.print("______");
		}
		System.out.print("_");
		System.out.println();
	}
	
	private void renderRow(Board board, int rowIndex) {
		System.out.print("  ");
		for (int i = 0; i < 8; i++) {
			System.out.print("|     ");
		}
		System.out.print("|");
		System.out.println();
		
		System.out.print((char)('A' + rowIndex));
		System.out.print(" ");
		
		for (int i = 0; i < 8; i++) {
			AbstractPiece pieceOnPosition = board.getPieceOnPosition(new BoardField(rowIndex, i));
			System.out.print("| " + PieceRenderer.renderPiece(pieceOnPosition) + " ");
		}
		System.out.print("|");
		System.out.println();
		System.out.print("  ");
		for (int i = 0; i < 8; i++) {

			System.out.print("|_____");
		}
		System.out.print("|");
		System.out.println();
	}
}
